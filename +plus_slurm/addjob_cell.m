function [ jobscluster ] = addjob_cell( jobscluster, f_name, varargin )
% obob_condor_addjob_cell Adds multiple jobs to the jobcluster using any
% possible combination of arguments it finds in cell arrays.
%
% Call as:
%       jobscluster = plus_slurm.addjob_cell( jobscluster, f_name, varargin )
%
% Everything is analogous to obob_condor_addjob. With one exception:
% Everytime, this function finds a cell array (like {1, 2, 3}), it will
% add one job for every possible combinations of all cell arrays.
%
% Suppose, we have a function called "calc_tf" that takes two arguments:
% The subject code and the frequency band. Now, if we do this:
%
%     subjects = {'aa123', 'bb345', 'cc567', 'dd9876', 'ee2345', 'ff4566'};
%     freqs = {[4 20], [20 60]};
%
%     jobscluster = obob_condor_addjob_cell(jobscluster, 'calc_tf', subjects, freqs);
%
% 12 jobs would be added to the jobcluster: one for every possible
% combination of subjects and freqs.
%
% For an example and further information, please refer to the help of the
% obob_condor_create and obob_condor_addjob functions.

% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

args = obob_cellargs(varargin{:});
for i = 1:length(args)
  jobscluster = plus_slurm.addjob(jobscluster, f_name, args{i}{:});
end %for

end

