function rval = is_classy_job( f_name)
% checks whether the job is a class-based job

% Copyright (c) 2017, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

job_meta_class = meta.class.fromName(f_name);

if isempty(job_meta_class)
  rval = false;
  return;
end %if

if isempty(strmatch('plus_slurm.slurm_job', {job_meta_class.SuperclassList.Name}))
  error('If you supply a class, it must inherit from plus_slurm.slurm_job!');
end %if

rval = true;