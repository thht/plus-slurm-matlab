function [ output ] = get_reqmem( clusterid, procid )
% Copyright (c) 2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

comparestr = 'RequestMemory = ifThenElse(MemoryUsage =!= undefined,';

call = sprintf('LD_LIBRARY_PATH="" /usr/bin/condor_q -l %d.%d | grep ^RequestMemory', clusterid, procid);

[~, tmpout] = system(call);

if strncmp(tmpout, comparestr, length(comparestr))
  rem = tmpout;
  while ~isempty(rem)
    [tok, rem] = strtok(rem, ',');
  end %while
  output = tok(1:end-2);
else
  output = get_classad('RequestMemory', clusterid, procid);
end %if


end

