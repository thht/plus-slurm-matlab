function [jobscluster] = addjob(jobscluster, f_name, varargin)
% obob_condor_addjob Adds a job (i.e. a call to a function) to a
% jobscluster for later submission.
%
% Call as:
%       jobscluster = plus_slurm.addjob(jobscluster, f_name, param1, param2, param3, ...)
%
% jobscluster is the jobscluster that holds all the information needed
% for submission. Create it with obob_condor_create. Then it gets updated
% with every call to obob_condor_addjob. Eventually, supply the
% jobscluster to obob_condor_submit to initiate the submission of all the
% jobs you added.
%
% f_name is the name of the function as a string. Please only provide the
% function name here without anything added.
%
% Everything after the function name is optional and represents the input
% values for the function. You can provide as many input parameters as you
% want. There are no more restrictions on the type you can use for the
% parameters. Anything works now. But please do not use big matrices as all
% the parameters first get saved to the network share and then reloaded.
% This is rather inefficient.
%
% For an example and further information, please refer to the help of the
% obob_condor_create function.

% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

try
  tmp = nargin(str2func(f_name));
catch ME
  if strcmp(ME.identifier, 'MATLAB:narginout:functionDoesnotExist')
    error('The function you supplied cannot be found. Please make sure it is in the current path.');
  else
    rethrow ME;
  end %if
end %try
jobscluster.jobs(end+1).f_name = f_name;
jobscluster.jobs(end).args = varargin;

end

