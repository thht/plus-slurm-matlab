classdef (Abstract) slurm_job < handle
  properties(SetAccess=protected, GetAccess=public)
    arguments;
  end %properties
  
  methods
    function set_arguments(obj, varargin)
      obj.arguments = varargin;
    end %function
    
    function run = shall_run(obj, varargin)
      run = true;
    end %function
    
    function run = shall_run_private(obj)
      run = obj.shall_run(obj.arguments{:});
    end %function
    
    function run_private(obj)
      obj.run(obj.arguments{:});
    end %function
  
  end %methods
  
  methods (Abstract)
    run(obj);
  end %methods
  
end %classdef