function submit(jobscluster)
% obob_slurm_submit Submits the jobcluster to the HNC Condor.
%
% Just supply the jobscluster that has been created with
% plus_slurm.create and filled with plus_slurm.addjob and the jobs will
% be submitted.
%
% For an example and further information, please refer to the help of the
% plus_slurm.create and plus_slurm.addjob functions.

% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% get cfg out of the struct...
cfg = jobscluster.cfg;

%% initialize empty submit file....
subfile = [];

%% convert jobsdir to absolute path if necessary....
if ~strcmp(cfg.jobsdir(1), '/')
  cfg.jobsdir = fullfile(pwd, cfg.jobsdir);
end %if

%% generate an internal job-number...
if cfg.inc_jobsdir
  % initialize jobsdir
  if ~exist(cfg.jobsdir, 'dir')
    mkdir(cfg.jobsdir);
  end %if

  % first we count how many things are in the directory....
  n = length(dir(cfg.jobsdir))-1;

  % now we check whether that folder already exists....
  while true
    job_folder = fullfile(cfg.jobsdir, sprintf('%03d', n));
    if ~exist(job_folder, 'dir')
      mkdir(job_folder);
      break;
    end %if
    n = n + 1;
  end %while
else
  job_folder = cfg.jobsdir;
  if exist(job_folder, 'dir')
    % check whether folder is empty
    if length(dir(job_folder)) > 2
      error('Your cfg.jobsfolder is not empty. Please choose a different folder or delete it.');
    end %if
  else
    mkdir(job_folder);
  end %if
end %if

mkdir(fullfile(job_folder, 'log'));
mkdir(fullfile(job_folder, 'slurm'));
mkdir(fullfile(job_folder, 'slurm/+plus_slurm'));

%% assemble the important information for the job and put it in a matfile...
bad_jobs = [];
idx_matfile = 1;
for i = 1:length(jobscluster.jobs)
  job_info = jobscluster.jobs(i);

  [fun_path, ~, ~] = fileparts(which(job_info.f_name));
  if strcmp(fun_path(1), '/')
    job_info.fun_path = fun_path;
  else
    job_info.fun_path = [];
  end %if
   
  if is_classy_job(job_info.f_name)    
    job_class = str2func(job_info.f_name);
    job_object = job_class();
    job_object.set_arguments(job_info.args{:});
    
    if ~job_object.shall_run_private()
      bad_jobs(end+1) = i;
      continue;
    end %if   
  end %if

  save(fullfile(job_folder, 'slurm', sprintf('job%d', idx_matfile)), 'job_info');
  idx_matfile = idx_matfile + 1;
end %for

% remove bad jobs...
jobscluster.jobs(bad_jobs) = [];

%% copy the run_job function to the jobs folder....
[mpath, ~, ~] = fileparts(mfilename('fullpath'));
copyfile(fullfile(mpath, 'run_functions', '*'), fullfile(job_folder, 'slurm'));
copyfile(fullfile(mpath, 'private', '*'), fullfile(job_folder, 'slurm', 'private'));
copyfile(fullfile(mpath, 'slurm_job.m'), fullfile(job_folder, 'slurm/+plus_slurm'));

%% prepare sub file....
matlabexec = cfg.matlab_exec;
matlabargs = {};
matlabargs{end+1} = '-nodesktop';
if cfg.cpus == 1
  matlabargs{end+1} = '-singleCompThread';
end %if 

if ~cfg.java
  matlabargs{end+1} = '-nojvm';
end %if

quotes = '''';

matlabcommand = {};
matlabcommand{end+1} = 'try;';
matlabcommand{end+1} = ['addpath(' quotes fullfile(job_folder, 'slurm') quotes ');'];
matlabcommand{end+1} = ['run_job(' quotes job_folder quotes ');'];
matlabcommand{end+1} = 'catch err;disp(err.getReport());end;exit;';

matlabargs{end+1} = ['-r "'];
for i =1:length(matlabcommand)
  matlabargs{end} = [matlabargs{end} ' ' matlabcommand{i}];
end %for

matlabargs_final = [];
for i = 1:length(matlabargs)
  matlabargs_final = [matlabargs_final ' ' matlabargs{i}];
end %for
matlabargs_final = [matlabargs_final '"'];

subfile = {};
subfile{end+1} = sprintf('#!/bin/bash\n');
subfile{end+1} = sprintf('#SBATCH --cpus-per-task=%d\n', cfg.cpus);
subfile{end+1} = sprintf('#SBATCH --mem=%s\n', cfg.mem);
subfile{end+1} = sprintf('#SBATCH --array=1-%d\n', length(jobscluster.jobs));
subfile{end+1} = sprintf('#SBATCH --time=%d\n', cfg.request_time);
subfile{end+1} = sprintf('#SBATCH --qos=%s\n', cfg.qos);
if ~isempty(cfg.exclude_nodes)
  subfile{end+1} = sprintf('#SBATCH --exclude="%s"\n', cfg.exclude_nodes);
end %if
subfile{end+1} = sprintf('#SBATCH --output=%s/out_%%a.log\n\n', fullfile(job_folder, 'log'));

if ~isempty(cfg.container)
    final_exec = sprintf('APPTAINER_BIND="" %s %s %s', cfg.container_command, cfg.container, matlabexec');
else
    final_exec = matlabexec';
end %if
subfile{end+1} = sprintf('%s %s', final_exec, matlabargs_final);
%% write subfile...
fid = fopen(fullfile(fullfile(job_folder, 'slurm'), 'submit.sh'), 'wt');
for i = 1:length(subfile)
  fprintf(fid, '%s\n', subfile{i});
end %for

fclose(fid);

%% call submit...
if cfg.do_submit
  cmd = sprintf('/usr/bin/sbatch %s', fullfile(job_folder, 'slurm', 'submit.sh'));
  system(cmd);
end %if

end

