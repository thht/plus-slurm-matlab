# PLUS Slurm Matlab

A convenient way to submit slurm jobs to the SCC Pilot Cluster

## Migrating from the obob_ownft version

If you alread know the obob_ownft version, this is very much alike. There are two main differences:

1. You need to install this and add it to your Matlab path.
   1. Install it by either download it as a zip or clone it.
   2. Add it to the path like this: `addpath('/home/USERNAME/matlab_packages/plus-slurm-matlab')`
      1. Of course, make sure the path is correct. It should point to the path in which you see this `README.md`
2. It is now a matlab package. This means that all function and class names have changed:
   1. The prefix `obob_slurm` must be replaced by a reference to the package `plus_slurm`. So:
      1. `obob_slurm_create` becomes `plus_slurm.create`
      2. etc....

## A note if you do not use the SCC Pilot Cluster

This package is tailored to the SCC Pilot Cluster. More specifically, it runs all jobs inside a custom apptainer container. This is what you want at the SCC Pilot, but maybe not what you want on another cluster....