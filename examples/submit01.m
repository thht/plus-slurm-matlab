%% clear
clear all global
restoredefaultpath
addpath('/home/thht/git/plus-slurm-matlab/');

%% setup jobcluster
cfg = [];

jobcluster = plus_slurm.create(cfg);
%% add job
jobcluster = plus_slurm.addjob(jobcluster, 'job01');
jobcluster = plus_slurm.addjob(jobcluster, 'job02');

%% submit
plus_slurm.submit(jobcluster);